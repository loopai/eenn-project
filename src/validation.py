import pickle
import inspect
from pathlib import Path

import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import roc_auc_score

from models import make_embed_dims, get_model

WORKING_DIR = Path(inspect.getsourcefile(lambda: 0)).resolve().parent.parent
DATA_DIR = WORKING_DIR / 'data'
#pprint(list(map(str, DATA_DIR.iterdir())))

train = pd.read_csv(DATA_DIR / 'train.csv', index_col='id')
#test = pd.read_csv(DATA_DIR / 'test.csv')
X = train.drop('target', axis=1)
y = train.target.values

df_nunique = X.nunique()
all_names = X.columns
cat_names = [x for x, y in zip(df_nunique.index, df_nunique) if y > 2]
other_names = list(set(all_names).difference(set(cat_names)))
orig_dims = df_nunique[cat_names]
embed_dims = make_embed_dims(orig_dims, max_dim=10)

model = get_model(cat_names, other_names, orig_dims, embed_dims)
model.compile(loss='binary_crossentropy', optimizer='adam')

##############################################################################
# draw the model
##############################################################################
#from tensorflow.keras.utils import plot_model
#plot_model(model, show_shapes=True, to_file='model.png', dpi=192)
##############################################################################

##############################################################################
# Entity Embedding - Preprocessing & Validation
##############################################################################
X = X.apply(LabelEncoder().fit_transform)
X_list = [X[col].values for col in cat_names]
X_list.append(
        X[other_names].apply(lambda col: col.astype('float32')).values
)
model.fit(X_list, y, epochs=8, batch_size=128, verbose=1)
print('first lap!')
print(X_list)

y_pred = model.predict(X_list)
print('second lap!')
print(y_pred)
print('y_pred.shape:', y_pred.shape)

print(roc_auc_score(y, y_pred))
##############################################################################
# Extracting embedded layers
##############################################################################
#embedded_layers_dict = {
#        name: model.get_layer(f'{name}_embedded').get_weights()[0]
#        for name in cat_names
#}
#with open('./embedded_layers_dict.pkl', 'wb') as pickle_file:
#    pickle.dump(embedded_layers_dict, pickle_file)
##############################################################################

