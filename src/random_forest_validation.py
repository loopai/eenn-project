import pickle
import inspect
from pathlib import Path

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score

RANDOM_STATE = 42
WORKING_DIR = Path(inspect.getsourcefile(lambda: 0)).resolve().parent.parent
DATA_DIR = WORKING_DIR / 'data'

train = pd.read_csv(DATA_DIR / 'train.csv', index_col='id')
X = train.drop('target', axis=1)
y = train.target.values

df_nunique = X.nunique()
cat_names = [x for x, y in zip(df_nunique.index, df_nunique) if y > 2]
other_names = list(set(X.columns).difference(set(cat_names)))
n_obs = X.shape[0]

##############################################################################
# Entity Embedding
##############################################################################
X_encoded = X.apply(LabelEncoder().fit_transform)
with open('./embedded_layers_dict.pkl', 'rb') as pickle_file:
    embedded_layers_dict = pickle.load(pickle_file)
for name in cat_names:
    columns_tmp = []
    for idx in range(n_obs):
        embedded_layer = embedded_layers_dict[name]
        idx_embedding = X_encoded[name][idx]
        columns_tmp.append(embedded_layer[idx_embedding])
    tmp = pd.DataFrame(
        data=columns_tmp,
        columns=[
            f'{name}_embedded_{num}' for num in range(len(embedded_layer[0]))
        ]
    )
    X_encoded = X_encoded.join(tmp).drop(name, axis=1)
X_encoded.reset_index(drop=True, inplace=True)

X_train, X_test, y_train, y_test = train_test_split(
    X_encoded, y, test_size=0.20, random_state=RANDOM_STATE
)

##############################################################################
# Entity Embedding - Best model performance
print('Enitity Embedding - Random Forest')
##############################################################################
classifier = RandomForestClassifier(n_jobs=-1, random_state=RANDOM_STATE)
param_distributions= {
    'n_estimators': [125, 175],
    'max_features': ['sqrt', 'log2'],
    'max_depth': [10, 20, None],
    'min_samples_split': [2, 6]
}
GSCV = RandomizedSearchCV(
    estimator=classifier,
    param_distributions=param_distributions,
    n_iter=5,
    scoring='roc_auc',
    n_jobs=-1,
    cv=4,
    verbose=1,
    random_state=RANDOM_STATE
)
GSCV.fit(X_train, y_train)
print(GSCV.best_params_)
##############################################################################
y_train_pred = GSCV.predict(X_train)
y_test_pred = GSCV.predict(X_test)
print('auc train: ', roc_auc_score(y_train, y_train_pred))
print('auc test: ', roc_auc_score(y_test, y_test_pred))
cr_train = classification_report(y_train, y_train_pred, output_dict=True)
cr_test = classification_report(y_test, y_test_pred, output_dict=True)

pd.DataFrame(cr_train).to_latex(WORKING_DIR / 'presentation' / 'figures' / 'cr_train_ee.tex')
pd.DataFrame(cr_test).to_latex(WORKING_DIR / 'presentation' / 'figures' / 'cr_test_ee.tex')
print('#'*79)
##############################################################################

##############################################################################
# One Hot Encoding
##############################################################################
ohe_names = [x for x, y in zip(df_nunique.index, df_nunique) if y > 2 and y < 50]
X_ohe = X.loc[:, ohe_names + other_names]
enc = OneHotEncoder()
X_ohe = X_ohe.join(
    pd.DataFrame(enc.fit_transform(X_ohe.loc[:, ohe_names + other_names]).toarray())
).drop(ohe_names + other_names, axis=1)
X_ohe.reset_index(drop=True, inplace=True)
X_ohe.head()

X_train, X_test, y_train, y_test = train_test_split(
    X_ohe, y, test_size=0.20, random_state=RANDOM_STATE
)
##############################################################################
# One Hot Encoding - Best model performance
print('One Hot Encoding - Random Forest')
##############################################################################
classifier = RandomForestClassifier(n_jobs=-1, random_state=RANDOM_STATE)
param_distributions= {
    'n_estimators': [125, 175],
    'max_features': ['sqrt', 'log2'],
    'max_depth': [10, 20, None],
    'min_samples_split': [2, 6]
}
GSCV = RandomizedSearchCV(
    estimator=classifier,
    param_distributions=param_distributions,
    n_iter=5,
    scoring='roc_auc',
    n_jobs=-1,
    cv=4,
    verbose=1,
    random_state=RANDOM_STATE
)
GSCV.fit(X_train, y_train)
print(GSCV.best_params_)
##############################################################################
y_train_pred = GSCV.predict(X_train)
y_test_pred = GSCV.predict(X_test)
print('auc train: ', roc_auc_score(y_train, y_train_pred))
print('auc test: ', roc_auc_score(y_test, y_test_pred))
cr_train = classification_report(y_train, y_train_pred, output_dict=True)
cr_test = classification_report(y_test, y_test_pred, output_dict=True)

pd.DataFrame(cr_train).to_latex(WORKING_DIR / 'presentation' / 'figures' / 'cr_train_ohe.tex')
pd.DataFrame(cr_test).to_latex(WORKING_DIR / 'presentation' / 'figures' / 'cr_test_ohe.tex')
print('#'*79)
##############################################################################

